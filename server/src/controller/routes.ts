import {Router} from 'express';
import * as api from './api';

export const router = Router();
export const path = '';

export const router2 = Router();


router.get('/units',api.getterUnits);
router.get('/units/:unitid', api.getterUnitById);
router.get('/atts/:attid',api.getterAttById);
router.get('/atts', api.getterAllAtts);
router.get('/attsofunit/:unitid',api.getterAttsByUnitId);

router.get('/patient',api.getterPatient);
router.get('/patient/:patientid',api.getterPatientById);
router.get('/patientcpf/:cpf',api.getterPatientBycpf)

router.get('/patient',api.authMiddleware,api.getterPatient);

router.get('/male',api.getterMaleAtts);
router.get('/female',api.getterFemaleAtts);
router.get('/ages', api.getterPacientAges);
router.get('/time/:id', api.getterAttTime);


router.post('/units',api.authMiddleware,api.createUnit);
router.post('/atts',api.authMiddleware,api.createAtt);
router.post('/patient',api.authMiddleware,api.createPatient);

router.patch('/units/:unitid', api.authMiddleware,api.patchUnit);
router.patch('/atts/:attid', api.authMiddleware,api.patchAtt);

router.delete('/units/:unitid', api.authMiddleware,api.deleteUnit);
router.delete('/atts/:attid',api.authMiddleware, api.deleteUnit);

router2.post('/auth/register',api.createUser);
router.post('/auth/authenticate',api.authUser);

router2.use(api.authMiddleware);
router2.get('/auth/atts2',api.getterAllAtts);

