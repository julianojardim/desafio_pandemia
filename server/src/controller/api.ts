import express from 'express';
import {AttendanceRepo} from '../persistencia/Repository/attendanceRepository';
import {PatientRepo} from '../persistencia/Repository/patientRepository';
import {UnitRepo} from '../persistencia/Repository/unitRepository';
import { Attendance } from '../entidades/attendance';
import {Users} from '../persistencia/Model/users';
import { User } from '../entidades/user';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import {default as authConfig} from '../config/auth'
import { HealthUnit } from '../entidades/healthUnit';
import { Patient } from '../entidades/patient';

//getters
export async function getterUnits(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const units:HealthUnit[] = await UnitRepo.getUnit(); // chama funcão que devolve todas unidades do BD
        res.json(units);
    } catch (err) {
        next(err);
    }
}
export async function getterUnitById(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string = req.params.unitid;
        const unit:HealthUnit[] = await UnitRepo.getUnitById(id);

        if(unit !== null){
            res.status(200);
            res.send(unit);
        }else{
            res.send('Unit not found');
            res.status(404);
        }
    } catch(err) {
        next(err)
    }
}
export async function getterAttsByUnitId(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string = req.params.unitid;
        const andamento:boolean =req.query.andamento == "true";
        const attendances:Attendance[] = await AttendanceRepo.getAttendanceByUnitId(id,andamento); // chama funcão que devolve todos atendimentos de uma unidade do BD
        res.json(attendances);
    } catch (err) {
        next(err);
    }
}
export async function getterAllAtts(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const attendances:Attendance[] = await AttendanceRepo.getAttendance(); // chama funcão que devolve todos atendimentos do BD
        res.json(attendances);
    } catch (err) {
        next(err);
    }
}
export async function getterMaleAtts(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void>{
    try {
        const auxAtts:Attendance[ ]= await AttendanceRepo.getAttendance();
        const maleAtts:Attendance[] = auxAtts.filter(att => {return att.paciente.sexo == "Masculino"});
        res.json(maleAtts);
    } catch (err){
        next(err);
    }
}
export async function getterFemaleAtts(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void>{
    try {
        const auxAtts:Attendance[]= await AttendanceRepo.getAttendance();
        const femaleAtts:Attendance[] = auxAtts.filter(att => {return att.paciente.sexo == "Feminino"});
        res.json(femaleAtts);
    } catch (err){
        next(err);
    }
}
export async function getterPacientAges(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void>{
    try {
        //const id = req.params.id;
        //const age:number[] = [];
        //const unit:number[] =[];
        let obj:any[] = [];
        const att:Attendance[]= await AttendanceRepo.getAttendance();
        for(let a of att){
            obj.push({id: a.idUnit,age: a.paciente.idade,resultTest1: a.resultTest1,resultTest2: a.resultTest2, dateAttendance:a.dateAttendance});            
        }

        if(obj !== null){
            res.status(200);
            res.send(obj);
        }else{
            res.send('Attendance not found');
            res.status(404).end();
        }
    } catch(err) {
        next(err);
    }
}
export async function getterAttTime(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void>{
    try {
        
        let id:string  = req.params.id
        let hasPacient:boolean = req.query.paciente == 'true';
        const times:any[]= await AttendanceRepo.getTime(id,hasPacient);
     
        if(times !== null){
            res.status(200);
            res.send(times);
        }else{
            res.send('Attendance not found');
            res.status(404).end();
        }
    } catch(err) {
        next(err);
    }

}
export async function getterAttById(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string = req.params.attid;
        const att:Attendance | null = await AttendanceRepo.getByIdAttendance(id);

        if(att !== null){
            res.status(200);
            res.send(att);
        }else{
            res.send('Attendance not found');
            res.status(404).end();
        }
    } catch(err) {
        next(err);
    }
}

export async function getterPatient(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const pat:Patient[] = await PatientRepo.getPatient(); 
        res.json(pat);
    } catch (err) {
        next(err);
    }
}
export async function getterPatientById(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string  = req.params.patientid
        const pat:Patient | null = await PatientRepo.getPatientById(id);
        if(pat !== null){
            res.status(200);
            res.send(pat);
        } else{

            res.send("Paciente não encontrado");
            res.status(404).end();
        }
    }
    catch(err) {
        next(err);
    }
}
export async function getterPatientBycpf(req: express.Request,res:express.Response, next: express.NextFunction):Promise<void>{
    try {
        const cpf:string = req.params.cpf;
        
        const pat:Patient | null = await PatientRepo.getByCpf(cpf);
        
        if(pat !== null){
            res.status(200);
            res.send(pat);
        } else {
            
            res.status(404);
            res.send("Paciente não encontrado").end();
        }
    }
    catch(err){
        next(err);
    }
}



//post
export async function createUnit(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const unit:any = req.body;
        const postUnit:any =  await UnitRepo.createUnit(unit);

        res.send(postUnit);
    } catch(err) {
        next(err);
    }
}
export async function createAtt(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const att:any = req.body;
        const postAtt:Attendance =  await AttendanceRepo.createAttendance(att);

        res.send(postAtt);
    } catch(err) {
        next(err);
    }
}

export async function createPatient(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const pat:any = req.body;
        const postPat:Patient =  await PatientRepo.createPatient(pat);

        res.send(postPat);
    } catch(err) {
        next(err);
    }
}

//patch
export async function patchUnit(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string = req.params.unitid;
        const unit = req.body;
        const patchUnit:HealthUnit =  await UnitRepo.patchUnit(unit,id);

        res.send(patchUnit);
    } catch(err) {
        next(err);
    }
}
export async function patchAtt(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        //const unitid = req.params.unitid;
        const attid:string = req.params.attid;
        const att = req.body;
        const patchAtt:Attendance =  await AttendanceRepo.patchAttendance(att, attid);

        res.send(patchAtt);
    } catch(err) {
        next(err);
    }
}

//delete
export async function deleteUnit(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        const id:string = req.params.unitid;
        const deleteUnit:HealthUnit =  await UnitRepo.deleteUnit(id);

        res.send(deleteUnit);
    } catch(err) {
        next(err);
    }
}
export async function deleteAtt(req: express.Request, res: express.Response, next: express.NextFunction):Promise<void> {
    try {
        //const unitid = req.params.unitid
        const attid:string = req.params.attid;
        const deleteAtt:Attendance =  await AttendanceRepo.deleteAttendance(attid);

        res.send(deleteAtt);
    } catch(err) {
        next(err);
    }
}

export async function createUser(req: express.Request, res: express.Response, next: express.NextFunction){
    const {email} = req.body;
    try{
        if(await Users.findOne({email})) return res.status(400).send({error: 'User already exist'});
        const user:User = await Users.create(req.body);
        user.password = 'undefined';
        return res.send(user);

    }catch(err){
        return res.status(400).send({error: 'Registration failed'});
    }
}
export async function work(req: express.Request, res: express.Response, next: express.NextFunction){
    console.log('work');
}
export async function authUser(req: express.Request, res: express.Response, next: express.NextFunction){
    const {email,password} = req.body;
    const user = await Users.findOne({email}).select('+password');

    if(!user) return res.status(400).send({error: 'User not found'});
    //console.log(user.password,password);
    await bcrypt.compare(password,user.password,function(err, r){
         if(err){
            return res.status(400).send({error: 'Erro no login'});
         }
         if(r){
            const token = jwt.sign({id: user._id},authConfig.secret,{expiresIn: 86400});
            user.password = 'undefined';
            res.status(200).json({idtoken:token, user: user});            
         } else{
            return res.status(400).send({error: 'Invalid Password'});
         }
        
        });
        //return res.status(400).send({error: 'Invalid Password'});
         
}

export async function authMiddleware(req: express.Request, res: express.Response, next: express.NextFunction){
    const authHeader = req.headers.authorization;

    if(!authHeader) return res.status(401).send({error: 'No token provided'});
    const parts = authHeader.split(' ');
    if(!(parts.length === 2)) return res.status(401).send({error: 'Token Error'});
    const [scheme,token] = parts;
    if(!/^Bearer$/i.test(scheme)) return res.status(401).send({error: 'Token malformatted'});

    jwt.verify(token,authConfig.secret, (err,decoded)=>{
        if(err) return res.status(401).send({error: 'Token invalid'});
        req.params.userId = (<any>decoded)._id;
        return next();
    });
}
