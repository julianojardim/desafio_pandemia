import { Attendance } from './attendance';

export interface HealthUnit {
    idUnit?: number,
    nome: string,
    //attendances?: Attendance[],
    address?: {
        rua: string,
        numero: number,
        bairro: string,
        cidade: string
    }
}
    
