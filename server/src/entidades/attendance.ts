import { Patient } from './patient';

import { HealthUnit } from './healthUnit';

export interface Attendance {
    idUnit: number,
    paciente: Patient,
    idAttendance: string,
    dateAttendance: Date,
    time: number,
    possibContagio: boolean,
    resultTest1?: boolean,
    resultTest2?: boolean
}
