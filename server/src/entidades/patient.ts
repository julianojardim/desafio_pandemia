export interface Patient {
    nome: string,
    cpf: string,
    numContato?: string,
    idade: number,
    sexo: string,
    peso?: number,
    altura?: number,
    adress?: {
        rua: string,
        numero: number,
        bairro: string,
        cidade: string
    }
}