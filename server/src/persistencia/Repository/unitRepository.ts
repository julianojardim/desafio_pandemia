import { UnitModel, UnitDocument } from '../Model/healthUnitModel';
import { HealthUnit } from '../../entidades/healthUnit';
import { Error } from 'mongoose';
import { isNull } from 'util';

export class UnitRepo {
    //criar unidade e gera um id automaticamente
    static async createUnit(unit: HealthUnit): Promise<any> {
        let id = 1;
        let lastUnit = await UnitModel.findOne().select('idUnit').sort({'idUnit':-1}).exec();
        let newId= id + (lastUnit?.idUnit == null ? 0 : lastUnit.idUnit);
        unit.idUnit = newId;
        return await UnitModel.create(unit);
    }
    //buscar por unidadest
    static async getUnit(): Promise<HealthUnit[]> {
        let unit = await UnitModel.find().exec();
        return unit;
    }
    //buscar unidade por id
    static async getUnitById(id: String): Promise<HealthUnit[]> {
        let unitId = await UnitModel.find().where("_id").equals(id).exec();
        //console.log(unitId);
        return unitId;
    }
    //editar unidade
    static async patchUnit(unit: HealthUnit, id: string): Promise<HealthUnit> {
        let uni = await UnitModel.findById(id).exec();
        if (uni != null) {
            uni.nome = unit.nome;
            uni.address = unit.address;
            return uni.save();
        } else {
            throw new Error('Id inexistente');
        }
    }
    //remover unidade
    static async deleteUnit(id: string): Promise<HealthUnit> {
        let uni = await UnitModel.findById(id).exec();
        if(uni != null) {
            return uni.deleteOne();
        } else {
            throw new Error('Id inexistente');
        }
    }
}
