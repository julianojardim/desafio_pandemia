import { Patient } from '../../entidades/patient';
import { PatientModel } from '../Model/patientModel';
import { Error } from 'mongoose';

export class PatientRepo {
    //criar paciente
    static async createPatient(patient: Patient): Promise<Patient> {
        return await PatientModel.create(patient);
    }
    //busca todos pacientes
    static async getPatient(): Promise<Patient[]> {
        let patient = await PatientModel.find().exec();
        return patient;
    }
    //busca paciente pelo CPF
    static async getByCpf(cpf: string): Promise<Patient | null> {
        let patientFind = await PatientModel.findOne().where("cpf").equals(cpf).exec();
        return patientFind;
    }
   /**
    * Retorna o paciente com o id igual ao solicitado
    * @param id 
    * @returns Patient | null
    */
    static async getPatientById(id: string): Promise<Patient | null>{
        let patientFind =  await PatientModel.findOne().where("_id").equals(id).exec();
            return patientFind;
       
    }


    //editar paciente
    static async patchPatient(patient: Patient, cpf:string): Promise<Patient> {
        let pac = await PatientModel.findOne().where("cpf").equals(cpf).exec();
        if (pac != null) {
            pac.nome = pac.nome;
            pac.cpf = pac.cpf;
            pac.numContato = pac.numContato;
            pac.idade = pac.idade;
            pac.peso = pac.peso;
            pac.altura = pac.altura;
            pac.adress = pac.adress;
            return pac.save();
        } else {
            throw new Error('CPF inexistente')
        }
    }
    //deletar paciente
    static async deletePatient(cpf: string): Promise<Patient> {
        let pat = await PatientModel.findOne().where("cpf").equals(cpf).exec();
        if (pat != null) {
            return pat.deleteOne();
        } else {
            throw new Error('CPF inexistente');
        }
    
    }
}