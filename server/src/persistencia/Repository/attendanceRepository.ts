import { Attendance } from '../../entidades/attendance';
import { AttendanceModel } from '../Model/attendanceModel';
import { Error } from 'mongoose';

export class AttendanceRepo {
    //criar atendimento
    static async createAttendance(attendance: Attendance): Promise<Attendance> {
        return await AttendanceModel.create(attendance);
    }
    //buscar atendimentos
    static async getAttendance(): Promise<Attendance[]> {
        let attendances = await AttendanceModel.find().populate("paciente").exec();
        return attendances;
    }
    //Busca o tempo de determindade unidade
    static async getTime( id: any, patient? : boolean): Promise<any[]> {
        
        let times;
        if(patient){
            times = await AttendanceModel.find().populate('paciente').select('paciente time').where('idUnit').equals(id).exec();
        } else {
            times = await AttendanceModel.find().select('time').where('idUnit').equals(id).exec();
            
        }
       
       
        return times;
    }
    //busca atendimentos de uma unidade
    static async getAttendanceByUnitId(unitId: string, andamento?: boolean): Promise<Attendance[]>{
        let where  = andamento ? {"resultTest2":undefined} : {} ;
        let attendances = await AttendanceModel.find(where).populate("paciente").where("idUnit").equals(unitId).exec();
        return attendances;
    }
    //buscar por id
    static async getByIdAttendance(id: string): Promise<Attendance | null> {
        let attendanceFinded = await AttendanceModel.findOne().where("_id").equals(id).exec();
        if (attendanceFinded != null) {
            return attendanceFinded;
        } else {
            throw new Error('Id inexistente');
        }
        
    }
    //editar atendimento
    static async patchAttendance(attendace: Attendance, id: string): Promise<Attendance> {
        let atend = await AttendanceModel.findById(id).exec();
        if (atend != null) {
            atend.paciente = attendace.paciente;
            atend.dateAttendance = attendace.dateAttendance;
            atend.time = attendace.time;
            atend.possibContagio = attendace.possibContagio;
            atend.resultTest1 = attendace.resultTest1;
            atend.resultTest2 = attendace.resultTest2;
            return await atend.save();
        } else {
            throw new Error('Id inexistente');
        }
    }

    //remover atendimento
    static async deleteAttendance(id: string): Promise<Attendance> {
        let aten = await AttendanceModel.findById(id).exec();
        if(aten != null) {
            return aten.deleteOne();
        } else {
            throw new Error('Id inexistente');
        }
    }
}
