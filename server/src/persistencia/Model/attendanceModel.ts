import { Attendance } from '../../entidades/attendance';
import { Document, Schema, model, Model, SchemaTypes } from 'mongoose';
import { PatientModel } from './patientModel';
import { } from '../../entidades/healthUnit'
import { UnitModel } from './healthUnitModel';
import { types } from 'util';

interface AttendanceDocument extends Attendance, Document { }

const AttendanceSchema = new Schema({
  idUnit: {type: Number},
  paciente: { type: SchemaTypes.ObjectId, ref: 'Patient', required: true },
  idAttendance: { type: Number, required: true },
  dateAttendance: { type: Date },
  possibContagio: { type: Boolean, required: true },
  time: {type: Number,required: true},
  resultTest1: { type: Boolean, required: true },
  resultTest2: { type: Boolean }
});

export const AttendanceModel: Model<AttendanceDocument> = model<AttendanceDocument>('Attendance', AttendanceSchema, 'Atendimentos');