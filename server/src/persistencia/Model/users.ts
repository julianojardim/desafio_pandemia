import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { User } from '../../entidades/user';
interface UserDocument extends User, mongoose.Document {} 
const UserSchema = new mongoose.Schema({
    name:{
        type: String,
        require:true,
    },
    unitid:{
        type: Number,
        require: true,
    },
    email:{
        type: String,
        unique: true,
        required: true,
        lowercase: true,
    },
    password:{
        type: String,
        required: true,
        select: false,
    }
});
UserSchema.pre<UserDocument>("save",async function(next:any){
    const hash = await bcrypt.hash(this.password,10);
    this.password = hash;
    next();
})
export const Users:mongoose.Model<UserDocument> = mongoose.model<UserDocument>('Users', UserSchema);