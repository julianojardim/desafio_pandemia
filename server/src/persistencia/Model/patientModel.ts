import { Patient } from '../../entidades/patient';
import { Document, model, Schema, Model } from 'mongoose';

interface PatientDocument extends Patient, Document {} 

const AdressPatientSchema = new Schema({
    rua: String,
    numero: Number,
    bairro: String,
    cidade: String
}); 
const PatientSchema = new Schema({
    nome: { type: String, required: true, maxlength: 200 },
    cpf: { type: String, min: 0, max: 14},
    numContato: { type: String },
    idade: { type: Number,  min: 0 },
    sexo: { type: String,  min: 0 },
    peso: { type: String, min: 0 },
    altura: { type: Number, min: 0},
    adress: {type: AdressPatientSchema}
});

export const PatientModel: Model<PatientDocument> = model<PatientDocument>('Patient', PatientSchema, 'pacientes');