import { Schema, Document, Model, model, SchemaTypes } from 'mongoose';
import { HealthUnit } from '../../entidades/healthUnit'

export interface UnitDocument extends HealthUnit, Document {}

const AdressUnitSchema = new Schema({
    rua: { type: String, required: true },
    numero: { type: Number, required: true },
    bairro: { type: String, required: true },
    cidade: { type: String, required: true }
});
const UnitSchema = new Schema({
    idUnit: { type: Number, required: true },
    nome: { type: String, required: true, maxlength: 200 },
    address: { type: {AdressUnitSchema} }
});

export const UnitModel: Model<UnitDocument> = model<UnitDocument>('UnitHealth', UnitSchema, 'Unidades');