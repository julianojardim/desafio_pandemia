import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { PatientRepo } from '../persistencia/Repository/patientRepository';
import { Patient } from '../entidades/patient';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco
 */
beforeAll(async () => {
    await dbhandler.connect();
});


/**
 * Limpa o BD
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 *  Remove e fecha conexão com BD após os testes
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('PatientRepository', () => {
    const mockPatient = {
        nome: 'Ana Maria',
        cpf: '5454545',
        numContato: '999999',
        idade: 25,
        sexo: 'feminino',
        peso: 50,
        altura: 1.7,
        adress: {
            rua: 'rua bla',
            numero: 722,
            bairro: 'rio branco',
            cidade: 'Canoas'
        }
    };
    const mockPatientNomeInv = {
        nome: '',
        cpf: '5454545',
        numContato: '999999',
        idade: 25,
        sexo: 'feminino',
        peso: 50,
        altura: 1.7,
        adress: {
            rua: 'rua bla',
            numero: 722,
            bairro: 'rio branco',
            cidade: 'Canoas'
        }
    };
    describe('criar()', () => {
        test('Deve inserir um paciente sem erros', async () => {
            expect(async () => {
                await PatientRepo.createPatient(mockPatient);
            }).not.toThrow();
        });
        test('Requer paciente com nome válido', async () => {
            await expect(PatientRepo.createPatient(mockPatient))
                .rejects
                .toThrow(mongoose.Error.ValidationError);
        });
        test('Deve inserir um paciente e obter objeto inserido no bd', async () => {
            const resultPatient = await PatientRepo.createPatient(mockPatient);
            expect(resultPatient.nome).toEqual(mockPatient.nome);
            expect(resultPatient.cpf).toEqual(mockPatient.cpf);
            expect(resultPatient.numContato).toEqual(mockPatient.numContato);
            expect(resultPatient.idade).toEqual(mockPatient.idade);
            expect(resultPatient.sexo).toEqual(mockPatient.sexo);
            expect(resultPatient.peso).toEqual(mockPatient.peso);
            expect(resultPatient.altura).toEqual(mockPatient.altura);
            expect(resultPatient.adress).toEqual(mockPatient.adress);
        });
    });
    describe('buscar()', () => {
        test('Deve retornar coleção vazia', async () => {
            const resultpatients = await PatientRepo.getPatient();
            expect(resultpatients).toBeDefined();
            expect(resultpatients).toHaveLength(0);
        });
    });
    describe('delete()', () => {
        test('Deve deletar um pacietne', async () =>{
            const resultpat = await PatientRepo.deletePatient(mockPatient.cpf);
            expect(resultpat).toEqual(0);
        });
    });
    describe('Editar', () => {
        test('Deve editar um paciente', async () => {
            const result = await PatientRepo.patchPatient(mockPatient, mockPatient.cpf);
            expect(result.nome).toEqual(mockPatient.nome);
            expect(result.cpf).toEqual(mockPatient.cpf);
            expect(result.numContato).toEqual(mockPatient.numContato);
            expect(result.idade).toEqual(mockPatient.idade);
            expect(result.sexo).toEqual(mockPatient.sexo);
            expect(result.peso).toEqual(mockPatient.peso);
            expect(result.altura).toEqual(mockPatient.altura);
            expect(result.adress).toEqual(mockPatient.adress);
        });
    });
   
}); 