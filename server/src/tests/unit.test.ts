import mongoose, { mongo } from 'mongoose';
import * as dbhandler from './dbhandler';
import { PatientRepo } from '../persistencia/Repository/patientRepository';
import { Patient } from '../entidades/patient';
import { UnitRepo } from '../persistencia/Repository/unitRepository';
import { HealthUnit } from '../entidades/healthUnit';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco
 */
beforeAll(async () => {
    await dbhandler.connect();
});


/**
 * Limpa o BD
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 *  Remove e fecha conexão com BD após os testes
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('UnitRepository', () => {
    const mockUnit: HealthUnit = {
        idUnit: 222,
        nome: 'Unidade 01',
        address: {
            rua: 'Rua hehe',
            numero: 2323,
            bairro: 'sao jose',
            cidade: 'POrto Alegre'
        }
    };
    const mockUnitId: HealthUnit = {
        idUnit: ,
        nome: 'Unidade 02',
        address: {
            rua: 'Rua hehe',
            numero: 2323,
            bairro: 'sao jose',
            cidade: 'POrto Alegre'
        }
    };
    describe('criar()', () => {
        test('Deve criar uma unidade sem erros', async () => {
            expect(async () => {
                await UnitRepo.createUnit(mockUnit);
            }).not.toThrow();
        });
        test('Requer unidade com id valido', async () => {
            await expect(UnitRepo.createUnit(mockUnitId))
            .rejects
            .toThrow(mongoose.Error.ValidationError);
        });
    });
    test('Deve inserir uma unidade e obter um objeto no bd', async () => {
        const resultUnit = await UnitRepo.createUnit(mockUnit);
        expect(resultUnit.idUnit).toEqual(mockUnit.idUnit);
        expect(resultUnit.nome).toEqual(mockUnit.nome);
        expect(resultUnit.address).toEqual(mockUnit.address);
    });
});