import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { Attendance } from '../entidades/attendance';
import { PatientRepo } from '../persistencia/Repository/patientRepository';
import { Patient } from '../entidades/patient';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco
 */
beforeAll(async () => {
    await dbhandler.connect();
});


/**
 * Limpa o BD
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 *  Remove e fecha conexão com BD após os testes
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('AttendanceRepo', () => {
    const mockAtt: Attendance = {
        idUnit: 212,
        idAttendance: '554',
        paciente: {
            nome: 'Leo ahsuahs',
            cpf: '232343',
            numContato: '00000',
            idade: 25,
            sexo: 'masculino',
            peso: 50,
            altura: 1.7
        },
        possibContagio: true,
        resultTest1: true,
        resultTest2: true
    }
});
