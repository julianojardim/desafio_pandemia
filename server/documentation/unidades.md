# Unidades
As rotas sobre as unidades estão aqui.
> **Métodos a serem usados** : `GET | POST | PATCH | DELETE`
## Todos as unidades 
 Retorna todos as unidades.

> **URL** : `/units`
>
 >**Method** : `GET`
 >
 >**Autenticação** : `Não` 




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "5f3e03eee105de2fd60fca9b",
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Porto Alegre"
    },
    "idUnit": 1,
    "__v": 0
  }
]
 ```
****
## Retorna a unidade específica
 Retorna os valores de uma unidade específica.

>  **URL** : `/units/:idUnit`
>
 > **Method** : `GET`
> 
 >  **Autenticação** : `Não`


### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
```json
[
  {
    "_id": "5f3e03eee105de2fd60fca9b",
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Porto Alegre"
    },
    "idUnit": 1,
    "__v": 0
  }
]
 ```
***
## Registrar Unidade 
 Serve para criar uma nova unidade

> **URL** : `/atts`
>
 >**Method** : `POST`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
 ```json
{
    "nome": "string",
    "address": {
      "rua": "string",
      "numero": "string",
      "bairro": "string",
      "cidade": "string"
    }
}
 ```

 **Dados de exemplo**
 ```json
{
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Porto Alegre"
    },
}
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
{
    "_id": "5f3e03eee105de2fd60fca9b",
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Porto Alegre"
    },
    "idUnit": 1,
    "__v": 0
  },
 ```
****

## Editar Unidade 
 Serve para editar uma unidade

> **URL** : `/units/:idUnit`
>
 >**Method** : `PATCH`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
```json
{
    "nome": "string",
    "address": {
      "rua": "string",
      "numero": "string",
      "bairro": "string",
      "cidade": "string"
    }
}
 ```

 **Dados de exemplo**
 ```json
{
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Canoas"
    },
}
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
{
    "_id": "5f3e03eee105de2fd60fca9b",
    "nome": "Hospita Conceição",
    "address": {
      "rua": "Av Alberto Bins",
      "numero": "45",
      "bairro": "Centro",
      "cidade": "Canoas"
    },
    "idUnit": 1,
    "__v": 0
},
 ```
****
## Apagar unidade 
 Serve para apagar uma unidade.

> **URL** : `/units/:idUnit`
>
 >**Method** : `DELETE`
 >
 >**Autenticação** : `Sim`

 
 
 
 