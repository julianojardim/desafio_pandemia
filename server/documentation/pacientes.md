# Paciente
As rotas os pacientes estão aqui.
> **Métodos a serem usados** : `GET | POST | PATCH | DELETE`
## Todos os Pacientes 
 Retorna todos os pacientes.

> **URL** : `/patient`
>
 >**Method** : `GET`
 >
 >**Autenticação** : `Sim` 




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "5f3e0653e105de2fd60fca9d",
    "nome": "Joazinho",
    "idade": 25,
    "peso": "75",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "_id": "5f3e0653e105de2fd60fca9e",
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    },
    "__v": 0
  }
]
 ```
****
## Retorna um paciente específico
 Retorna os valores de um paciente específico.

>  **URL** : `/patient/:patientId`
>
 > **Method** : `GET`
> 
 >  **Autenticação** : `Não`


### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
```json
{
    "_id": "5f3e0653e105de2fd60fca9d",
    "nome": "Joazinho",
    "idade": 25,
    "peso": "75",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "_id": "5f3e0653e105de2fd60fca9e",
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    },
    "__v": 0
  }
 ```
***
## Registrar Paciente
 Serve para criar um novo paciente

> **URL** : `/atts`
>
 >**Method** : `POST`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
 ```json
{
    
    "nome": "string",
    "idade": "number",
    "peso": "number",
    "altura": "number",
    "numContato": "string",
    "sexo": "string",
    "cpf": "string",
    "adress": {
      "rua": "string",
      "numero": "number",
      "bairro": "string"
    }
  }
 ```

 **Dados de exemplo**
 ```json
{
    "nome": "Joazinho",
    "idade": 25,
    "peso": "75",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    }
  }
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
{
    "_id": "5f3e0653e105de2fd60fca9d",
    "nome": "Joazinho",
    "idade": 25,
    "peso": "75",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "_id": "5f3e0653e105de2fd60fca9e",
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    },
    "__v": 0
  }
 ```
****

## Editar paciente 
 Serve para editar um paciente.
> **URL** : `/pacient/:pacientId`
>
 >**Method** : `PATCH`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
```json
{
    "nome": "string",
    "idade": "number",
    "peso": "number",
    "altura": "number",
    "numContato": "string",
    "sexo": "string",
    "cpf": "string",
    "adress": {
      "rua": "string",
      "numero": "number",
      "bairro": "string"
    }
}
 ```

 **Dados de exemplo**
 ```json
{
    "nome": "Joazinho",
    "idade": 25,
    "peso": "85",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    }
  }
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
{
    "_id": "5f3e0653e105de2fd60fca9d",
    "nome": "Joazinho",
    "idade": 25,
    "peso": "75",
    "altura": 175,
    "numContato": "000000000",
    "sexo": "Masculino",
    "cpf": "011.011.011-11",
    "adress": {
      "_id": "5f3e0653e105de2fd60fca9e",
      "rua": "Av Principal",
      "numero": 305,
      "bairro": "Centro"
    },
    "__v": 0
  }
 ```
****
## Apagar unidade 
 Serve para apagar uma unidade.

> **URL** : `/patient/:patientId`
>
 >**Method** : `DELETE`
 >
 >**Autenticação** : `Sim`

 
 
 
 