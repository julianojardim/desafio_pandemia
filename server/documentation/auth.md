# Auth
Aqui que é registrado um usuário e efetuado o a autenticação do usuário.
> **Métodos a serem usados** : `GET | POST `

## Register 
 Serve para criar um novo usuário

> **URL** : `/auth/register`
>
 >**Method** : `POST`
 >
 >**Autenticação** : `Não`

 **Dados a serem enviados**
 ```json 
 {
     "email":"Email válido",
     "password":"Senha",
     "unitid":"Id de uma unidade",
     "name":"Nome do usuário"
 }
 
 ```

 **Dados de exemplo**
 ```json
 {
     "email":"joaoninguem@mail.com",
     "password":"senhaforte123",
     "unitid":2,
     "name":"João Ninguem"
     
 }
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
 {
     "email":"joaoninguem@mail.com",
     "password":"indefindo",
     "unitid":2,
     "name":"João Ninguem",
     
     
 }
 ```
****
## Authenticate
 Serve para criar um novo usuário

>  **URL** : `/auth/authenticate`
>
 > **Method** : `POST`
 >
>  **Autenticação** : `Não`

 **Dados a serem enviados**
 ```json 
 {
     "email":"Email válido",
     "password":"Senha",
 }
 
 ```

 **Dados de exemplo**
 ```json
 {
     "email":"joaoninguem@mail.com",
     "password":"senhaforte123",
 }
 ```
### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
 ```json
 {
  "idtoken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmM2UwMjgzZTEwNWRlMmZkNjBmY2E5OSIsImlhdCI6MTU5Nzk2NDUxOSwiZXhwIjoxNTk4MDUwOTE5fQ.Y7XcGCM54pfjjjHwzEFnNBT9Pj_DyrPmkrQBwunq0I8",
  "user": {
    "_id": "5f3e0283e105de2fd60fca99",
    "name": "João Ninguem",
    "unitid": 1,
    "email": "joaoninguem@mail.com",
    "password": "undefined",
    "__v": 0
  } 
 ```
 ### Resposta de Erro
 >**Code**: `400 BAD REQUEST`
 
 **Exemplo de resposta**
 ```json
 {
  "Error":"Invalid password"
} 
 ```

 
 
 