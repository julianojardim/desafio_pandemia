# Atendimentos
As rotas sobre os atendimentos estão aqui.
> **Métodos a serem usados** : `GET | POST | PATCH | DELETE`
## Todos os atendimentos 
 Retorna todos os atendimentos.

> **URL** : `/atts`
>
 >**Method** : `GET`
 >
 >**Autenticação** : `Não` 




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "string",
    "idUnit": "number",
    "idAttendance": "number",
    "dateAttendance": "Data",
    "paciente": "Patient | string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
],
 ```
****
## Atendimento específico
 Retorna o atendimento específico.

>  **URL** : `/atts/:attid`
>
 > **Method** : `GET`
> 
 >  **Autenticação** : `Não`



### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
 ```json
 {
    "_id": "string",
    "idUnit": "number",
    "idAttendance": "number",
    "dateAttendance": "Data",
    "paciente": "Patient | string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
}   
```
****
## Todos os atendimentos de uma Unidade
 Retorna todos os atendimentos de uma determinada unidade.

>  **URL** : `/attsofunit/:idUnit`
>
 > **Method** : `GET`
> 
 >  **Autenticação** : `Não`

**Parâmetros de URL**
 > **andamento** | boolean :  `Caso verdadeiro retorna apenas os atendimentos que ainda
 não contêm o segundo resultado.`
 




### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "string",
    "idUnit": "number",
    "idAttendance": "number",
    "dateAttendance": "Data",
    "paciente": "Patient | string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
]
```
****
## Atendimentos com a idade 
 Retorna todos os atendimentos contendo apenas a idade e os resultados

> **URL** : `/ages`
>
 >**Method** : `GET`
 >
 >**Autenticação** : `Não`




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    
    "age": "number",
    "dateAttendance": "Data",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
],
 ```
****
## Atendimentos Femininos
 Retorna todos os atendimentos em que o paciente é do sexo feminino.

> **URL** : `/female`
>
 >**Method** : `GET`
> 
 >**Autenticação** : `Não`




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "string",
    "idUnit": "number",
    "idAttendance": "number",
    "dateAttendance": "Data",
    "paciente": "Patient | string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
],
 ```
****
## Atendimentos Masculinos
 Retorna todos os atendimentos em que o paciente é do sexo masculino.

> **URL** : `/male`
>
 >**Method** : `GET`
 >
 >**Autenticação** : `Não`




### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "string",
    "idUnit": "number",
    "idAttendance": "number",
    "dateAttendance": "Data",
    "paciente": "Patient | string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
],
 ```
****
## Tempo
 Retorna o tempo de atendimento de cada unidade.

>  **URL** : `/time/:idUnit`
>
 > **Method** : `GET`
 >
 >  **Autenticação** : `Não`

**Parâmetros de URL**
 > **paciente** | boolean : `Caso verdadeiro retorna o paciente junto.`
 




### Resposta de sucesso

> **Code**:  `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "string",
    "paciente?": "Patient | string",
    "time": "number",
  }
]
```
****
## Registrar Atendimento 
 Serve para criar um novo atendimento

> **URL** : `/atts`
>
 >**Method** : `POST`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
 ```json
[
  {
    "idUnit": "number",
    "dateAttendance": "Data",
    "paciente": "string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
  }
],
 ```

 **Dados de exemplo**
 ```json
[
  {
    "idUnit": 1,
    "dateAttendance": Data,
    "paciente": "5f3e0653e105de2fd60fca9d",
    "time": 45,
    "possibContagio":true,
    "resultTest1": false,
  }
],
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
[
  {
    "_id": "5f3e065de105de2fd60fcaa0",
    "idUnit": 1,
    "dateAttendance": Data,
    "paciente": Pacient,
    "time": 45,
    "possibContagio":true,
    "resultTest1": false,
  }
],
 ```
****

## Editar Atendimento 
 Serve para editar um atendimento

> **URL** : `/atts/:attid`
>
 >**Method** : `PATCH`
 >
 >**Autenticação** : `Sim`

 **Dados a serem enviados**
 ```json
{
    "idUnit": "number",
    "dateAttendance": "Data",
    "paciente": "string",
    "time": "number",
    "possibContagio": "boolean",
    "resultTest1": "boolean",
    "resultTest2?": "boolean"
}
 ```

 **Dados de exemplo**
 ```json
  {
    "idUnit": 1,
    "dateAttendance": Data,
    "paciente": "5f3e0653e105de2fd60fca9d",
    "time": 45,
    "possibContagio":true,
    "resultTest1": true,
    "resultTest2":true
  }
 ```
### Resposta de sucesso

>**Code**: `200 OK`

**Exemplo de resposta**
 ```json
{
    "_id": "5f3e065de105de2fd60fcaa0",
    "idUnit": 1,
    "dateAttendance": Data,
    "paciente": Pacient,
    "time": 45,
    "possibContagio":true,
    "resultTest1": true,
    "resultTest1": true,
}

 ```
****
## Apagar  atendimento 
 Serve para apagar um atendimento

> **URL** : `/atts/:attsid`
>
 >**Method** : `DELETE`
 >
 >**Autenticação** : `Sim`

 

 
 
 