
# API Rest

Aqui consta a documentação dos serviços de nossa API

## Nossos Endpoints 

.

* [Auth](./documentation/auth.md) 

* [Atendimentos](./documentation/atendimentos.md) 

* [Unidades](./documentation/unidades.md) 

* [Pacientes](./documentation/pacientes.md)
