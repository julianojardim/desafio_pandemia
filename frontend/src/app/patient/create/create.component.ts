import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Patient} from '../../patient';
import {ApiService} from '../../service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  paciente: Patient;
  isLoading = false;
  formPatient: FormGroup = this.fb.group({
    nome: [''],
    idade: [''],
    peso: [''],
    altura: [''],
    numContato: ['', [Validators.pattern('^(\\d)+?$')]],
    sexo: [''],
    cpf: [null, [Validators.required, Validators.pattern('(\\d{3})[.](\\d{3})[.](\\d{3})[-](\\d{2})')]]
  });
  formAddress: FormGroup = this.fb.group({
    rua: ['', Validators.required],
    numero: ['', Validators.required],
    complemento: ['', Validators.required],
    bairro: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private router: Router
  ) {

  }

  get nome() {
    return this.formPatient.get('nome');
  }

  get cpf() {
    return this.formPatient.get('cpf');
  }

  get peso() {
    return this.formPatient.get('peso');
  }

  get sexo() {
    return this.formPatient.get('sexo');
  }

  get numContato() {
    return this.formPatient.get('numContato');
  }

  get idade() {
    return this.formPatient.get('idade');
  }

  get altura() {
    return this.formPatient.get('altura');
  }

  get bairro() {
    return this.formAddress.get('bairro');
  }

  get rua() {
    return this.formAddress.get('rua');
  }

  get numero() {
    return this.formAddress.get('numero');
  }

  get complemento() {
    return this.formAddress.get('complemento');
  }

  submit() {
    this.isLoading = true;
    this.paciente = this.formPatient.value;
    this.paciente.adress = this.formAddress.value;
    this.api.setPatient(this.paciente).subscribe(
      (paciente: Patient) => {
        this.isLoading = false;
        this.router.navigate(['/saude/atendimento', {id : paciente._id}]);
      }
    );
  }

  ngOnInit() {
  }


}
