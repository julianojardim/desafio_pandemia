import {Component, OnInit} from '@angular/core';
import {ApiService} from '../service';
import {HealthUnit} from '../healthUnit';
import {Attendance} from '../attendance';

export interface UnitHasCase {
  name: string;
  case: number;
  idUnit;
  _id: string;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  units: HealthUnit[];
  atts: Attendance[];
  displayed = ['name', 'case'];
  unitsHasCases: UnitHasCase[] = [];
  UnitName = 'carregando ...';
  UnitId: any = 1;
  times: any[];
  timeResult = {min: 0, max: 0, med: 0};
  weekly = true;

  constructor(private service: ApiService) {
  }

  ngOnInit() {
    this.getUnits();
  }

  /**
   * Pega todas as unidades
   */
  getUnits() {
    this.service.getUnits()
      .subscribe(
        (units: HealthUnit[]) => {
          this.units = units;
          this.UnitName = units[0].nome;
          this.getAtts();


        });
  }

  /**
   * Pega os atendimentos
   */
  getAtts() {
    this.service.getAtts().subscribe((atts: Attendance[]) => {
        this.atts = atts;
        this.calcCases();
      }
    );
  }

  calcCases() {

    this.units.forEach((unit: HealthUnit) => {
      const atts: Attendance[] = this.atts.filter((att) => att.idUnit == unit.idUnit);
      const positive = this.getPositiveCases(atts);
      this.unitsHasCases.push(
        {name: unit.nome, case: positive, idUnit: unit.idUnit, _id: unit._id}
      );
    });
    this.unitsHasCases.sort((a, b) => {
      if (a.case < b.case) {
        return 1;
      } else if (a.case > b.case) {
        return -1;
      }
      return 0;
    });
  }

  calcTimes(times) {
    let min = times[0].time;
    let max = times[0].time;
    let total = 0;

    for (const t of times) {
      if (t.time < min) {
        min = t.time;
      }
      if (t.time > max) {
        max = t.time;
      }
      total += t.time;
    }
    const med = Math.round(total / times.length);
    console.log(min,max,med);
    return {min:min, med:med, max:max};
  }

  getTimes(id: string) {
    this.service.getTimes(id).subscribe(data => {

      this.timeResult = this.calcTimes(data);
    });
  }

  getPositiveCases(atts): number {// conta o numero de casos positivos da atual attsUnit
    let positiveCases = 0;
    for (const a of atts) {
      if (a.resultTest1) {
        if (a.resultTest2 == undefined) {
          positiveCases++;
        } else {
          if (a.resultTest2) {
            positiveCases++;
          }
        }
      } else if (a.resultTest2) {
        positiveCases++;
      }
    }
    return positiveCases;
  }

  setUnit(row: UnitHasCase) {
    console.log(row);
    this.UnitName = row.name;
    this.UnitId = row.idUnit;
    this.timeResult = {min : 0 , med: 0 , max: 0};
    this.getTimes(row.idUnit);
  }

}
