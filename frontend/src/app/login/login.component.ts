import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { ApiService } from '../service';
import {AppComponent} from '../app.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login = false

  constructor(private fb: FormBuilder, private router: Router,private apiService: ApiService,private app:AppComponent) {

  }
  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });

  getEmail() {return this.loginForm.get('email'); }

  getPassword() {return this.loginForm.get('password'); }
  ngOnInit() {
  }
  onSubmit() {
    try{
      this.apiService.postLogin({email: this.getEmail().value, password: this.getPassword().value}).subscribe(data =>{
        let idtoken = data.idtoken;
        localStorage.setItem('token', idtoken);
        let user = data.user.name;
        localStorage.setItem('user',user);
        localStorage.setItem('unitid',data.user.unitid);
        this.app.OnLogin();
        this.router.navigate(['saude']);
      });
    }catch(err){
      alert('erro');
      console.log(err);
    }
     
  }

}
