import { Component } from '@angular/core';
import {ApiService} from './service';
import { DataSharingService } from './data-sharing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Desafio Pandemia';
  isChecked = false;
  isLogged:boolean= this.apiService.isLogged();
  
  constructor(private apiService: ApiService,private dataSharingService: DataSharingService){
    console.log('logado:',this.isLogged);
    this.dataSharingService.isUserLoggedIn.subscribe( value => {
    this.isLogged = value;
    console.log('logado:',this.isLogged);
  });
  }
  OnLogout(){
    this.apiService.logout();
    this.dataSharingService.isUserLoggedIn.next(false);
  }
  OnLogin(){
    this.dataSharingService.isUserLoggedIn.next(true);
  }


}

