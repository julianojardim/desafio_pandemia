import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Patient} from '../../patient';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Attendance} from "../../attendance";
import {MatDialog} from "@angular/material/dialog";
import {DialogPatientComponent} from "../../dialog-patient/dialog-patient.component";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  consultations;
  searchForm: FormGroup = this.fb.group({
    cpf: [null, [Validators.required, Validators.pattern('(\\d{3})[.](\\d{3})[.](\\d{3})[-](\\d{2})')]]
  });
  displayedAttendance: string[] = ['patient', 'time'];
  displayedAndamento: string[] = ['patient'];
  l;
  idUnit = localStorage.getItem('unitid');
  attendances: Attendance[];

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private route: Router,
    private activate: ActivatedRoute,
    private snack: MatSnackBar,
    private dialog:MatDialog
  ) {
    activate.paramMap.subscribe(
      (param: ParamMap) => {
        if (param.get('saved')) {
          snack.open('Atendimento salvo com sucesso', 'Fechar', { duration: 5000});
        }

      });
  }

  get cpf() {
    return this.searchForm.get('cpf');
  }

  onSubmit() {
    this.apiService.getPatientCpf(this.cpf.value).subscribe(
      (patient: Patient) => {
        this.route.navigate(['saude/atendimento', {id: patient._id}]);
      },
      (err) => {
        if (err === 404) {
          this.route.navigate(['paciente/cadastrar']);
        }
      }
    );
  }
  openDialog(att:Attendance){
    let dialogRef = this.dialog.open(DialogPatientComponent,{
      data:att
    });
    dialogRef.afterClosed().subscribe(
      (id)=>{ this.attendances.filter((att)=>att._id != id)}
    )
  }
  ngOnInit() {
    this.getAttendance();
    this.getAttendanceWithoutResult2();
  }
  getAttendance() {
    this.apiService.getTimes(this.idUnit, true).subscribe(
      (data) => {
        this.consultations = data;
      }
    );
  }
  getAttendanceWithoutResult2() {
    this.apiService.getAttsofUnit(this.idUnit,true).subscribe(
      (data)=>{
        this.attendances = data;
      }
    )
  }
}
