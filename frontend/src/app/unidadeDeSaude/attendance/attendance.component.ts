import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {ApiService} from '../../service';
import {Attendance} from '../../attendance';
import {Patient} from '../../patient';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  attForm: FormGroup;
  attendance;
  data: Date;
  isLoading = true;
  paciente: Patient;
  start;
  end;
  time;
  error = false;
  success = true;

  constructor(private fb: FormBuilder, private apiService: ApiService, private route: ActivatedRoute, private router: Router) {
    this.attForm = this.fb.group({
      possibCont: ['', Validators.required],
      result1: ['', Validators.required],
      result2: ['']
    });
    route.paramMap.subscribe(
      (params: ParamMap) => {
        apiService.getPatientId(params.get('id')).subscribe(
          (paciente: Patient) => {
            this.paciente = paciente;
            this.isLoading = false;
          }
        );
      }
    );
  }

  get possibCont() {
    return this.attForm.get('possibCont').value == 'true';
  }

  get result1() {
    return this.attForm.get('result1').value == 'true';
  }
  get result2() {
    return this.attForm.get('result2').value;
  }

  ngOnInit() {
    this.start = Date.now();
  }

  onSubmit() {
    this.end = Date.now();
    const data = new Date();
    console.log(data);
    this.time = Math.ceil((this.end - this.start) / 1000 / 60);
    let att: Attendance = {
      idUnit: Number(localStorage.getItem('unitid')),
      idAttendance: 1,
      dateAttendance: data,
      paciente: this.paciente._id,
      time: this.time,
      possibContagio: this.possibCont,
      resultTest1: this.result1

    };
    if(this.result2 == "") {

    }else {
      att.resultTest2 = this.result2 == "true";
    }

    this.isLoading = true;
    this.apiService.setAtts(att).subscribe(
      () => {
        this.isLoading = false;
        this.router.navigate(['/saude', {saved: true}]);
      },
      () => {
        this.error = true;
      }
    );


  }


}
