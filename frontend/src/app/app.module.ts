
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { CreateComponent } from './patient/create/create.component';
import {FormsModule, ReactiveFormsModule, FormControlName} from '@angular/forms';
import { CpfComponent } from './cpf/cpf.component';
import { DashboardComponent as DashSaude} from './unidadeDeSaude/dashboard/dashboard.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HttpClient, HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';
import { AttendanceComponent } from './unidadeDeSaude/attendance/attendance.component';
import { CovidcasesComponent } from './covidcases/covidcases.component';
import {MatSelectModule} from '@angular/material/select';
import { AtttimeComponent } from './atttime/atttime.component';
import { MatCheckboxModule } from '@angular/material';
import {CookieService} from 'ngx-cookie-service';
import { TokenInterceptorService } from './token-interceptor.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { DialogPatientComponent } from './dialog-patient/dialog-patient.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    CreateComponent,
    CpfComponent,
    DashSaude,
    DashboardComponent,
    AttendanceComponent,
    CovidcasesComponent,
    AtttimeComponent,
    DialogPatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    FormsModule
  ],
  entryComponents:[
    DialogPatientComponent
  ],
  providers: [CookieService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
