import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ApiChart} from '../chart';
import {HealthUnit} from '../healthUnit';
import {ApiService} from '../service';
import {Attendance} from '../attendance';
import {FormBuilder, FormControl} from '@angular/forms';

@Component({
  selector: 'app-covidcases',
  templateUrl: './covidcases.component.html',
  styleUrls: ['./covidcases.component.css']
})
export class CovidcasesComponent implements OnInit, OnChanges {
  units: HealthUnit[];
  unitNameByid: HealthUnit;
  @Input('UnitId') filteredUnitId: number;
  @Input('weekly') weekly: boolean;
  atts: Attendance[];
  attsUnit: Attendance[];
  attsMaleUnit: Attendance[];
  maleAtts: Attendance[];
  attsFemaleUnit: Attendance[];
  femaleAtts: Attendance[];
  positiveAtts: Attendance[];
  ages: any[];
  unitIdForm;
  isLoading = true;


  constructor(private apiChart: ApiChart, private apiService: ApiService, private formBuilder: FormBuilder) {
    this.unitIdForm = this.formBuilder.group({
      name: new FormControl('')
    });
  }

  async ngOnInit() {

    this.getUnits();




    this.apiChart.plotChart();
    this.apiChart.plotChartSex();
    this.apiChart.plotChartAges();

  }

  ngOnChanges() {
    this.onSubmit();
  }

  getUnits() {
    this.apiService.getUnits().subscribe(data => {
      this.units = data;
      this.getAtts();
    });
  }

  getAtts() {
    this.apiService.getAtts().subscribe(data => {
      this.atts = data;
      this.getMaleAtts();
    });
  }

  getMaleAtts() {
    this.apiService.getMaleAtts().subscribe(data => {
      this.maleAtts = data;
      this.getFemaleAtts();
    });
  }

  getFemaleAtts() {
    this.apiService.getFemaleAtts().subscribe(data => {
      this.femaleAtts = data;
      this.getAges();
    });
  }

  getAges() {
    this.apiService.getAges().subscribe(data => {

      this.ages = data;
      this.isLoading = false;
      this.onSubmit();
    });

  }


  onSubmit() {
    if (!this.isLoading) {


      // console.log(e);
      // const filteredUnit = this.units.filter( a => {if(a.nome == e.name) return a.nome; });
      // console.log(filteredUnit);
      this.attsUnit = this.atts.filter((att) => att.idUnit == this.filteredUnitId);
      if(this.weekly){
        this.attsUnit = this.attsUnit.filter(att =>{
          return this.isWeeklyAtt(att);
        });
      }
      // console.log(this.attsUnit);
      this.attsFemaleUnit = this.femaleAtts.filter((att) => att.idUnit == this.filteredUnitId);
      this.attsMaleUnit = this.maleAtts.filter((att) => att.idUnit == this.filteredUnitId);
      this.apiChart.updateChart(this.getPositiveCases(), this.getNegativeCases());
      this.apiChart.updateChartS(this.getMaleCases(), this.getFemaleCases());
      this.apiChart.updateChartAges(this.getArrayAges());
    }
  }


  getPositiveCases(): number {// conta o numero de casos positivos da atual attsUnit
    let positiveCases = 0;
    for (let a of this.attsUnit) {
      if (a.resultTest1) {
        if (a.resultTest2 == undefined) {
          positiveCases++;
        } else {
          if (a.resultTest2) {
            positiveCases++;
          }
        }
      } else if (a.resultTest2) {
        positiveCases++;
      }
    }
    return positiveCases;
  }

  getNegativeCases(): number {
    let negativeCases = 0;
    for (let a of this.attsUnit) {
      if (!a.resultTest1) {
        if (a.resultTest2 == undefined) {
          negativeCases++;
        } else {
          if (a.resultTest2 == false) {
            negativeCases++;
          }
        }
      } else {
        if (a.resultTest2 == false) {
          negativeCases++;
        }
      }
    }
    return negativeCases;
  }
  isWeeklyAtt(att:Attendance){
    let now:any = Date.now();
    let auxDate:any = new Date(att.dateAttendance);
    console.log(now,auxDate);
    let diff = Math.round(((now-auxDate) / 86400000));
    console.log('diff',diff);
    if(diff<7) return true;

    return false;
  }
  getPositiveAtts() {
    this.positiveAtts = this.attsUnit.filter(att => this.filterPositiveCases(att));
    console.log(this.positiveAtts);
  }

  filterPositiveCases(a: Attendance): boolean {
    if (a.resultTest1) {
      if (a.resultTest2 == undefined) {
        return true;
      } else {
        if (a.resultTest2) {
          return true;
        }
      }
    } else if (a.resultTest2) {
      return true;
    }
  }

  filterNegativeCases(a: Attendance): boolean {
    if (!a.resultTest1) {
      if (a.resultTest2 == undefined) {
        return true;
      } else {
        if (a.resultTest2 == false) {
          return true;
        }
      }
    } else {
      if (a.resultTest2 == false) {
        return true;
      }
    }

  }

  getMaleCases(): number {
    let cases: number;
    this.getPositiveAtts();
    const auxAtts: Attendance[] = this.positiveAtts.filter(att => att.paciente.sexo == 'Masculino');
    cases = auxAtts.length;
    return cases;
  }

  getFemaleCases(): number {
    let cases: number;
    this.getPositiveAtts();
    const auxAtts: Attendance[] = this.positiveAtts.filter(att => {
      return att.paciente.sexo == 'Feminino';
    });
    console.log(auxAtts);
    cases = auxAtts.length;
    return cases;
  }

  getArrayAges(): any {
    let auxAges;


    let auxAgesPositive;
    let auxAgesNegative;
    let arrayAgesPositive: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    let arrayAgesNegative: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    auxAges = this.ages.filter(ages => ages.id == this.attsUnit[0].idUnit);
    if(this.weekly){
      auxAges= auxAges.filter(att => {
        return this.isWeeklyAtt(att);
      });}
    auxAgesPositive = auxAges.filter(ages => this.filterPositiveCases(ages));
    console.log('positive', auxAges);
    console.log('positive', auxAgesPositive);
    auxAgesNegative = auxAges.filter(ages => this.filterNegativeCases(ages));
    for (let e of auxAgesPositive) {
      if (e.age <= 9) {
        arrayAgesPositive[0]++;
      } else if (e.age <= 19) {
        arrayAgesPositive[1]++;
      } else if (e.age <= 29) {
        arrayAgesPositive[2]++;
      } else if (e.age <= 39) {
        arrayAgesPositive[3]++;
      } else if (e.age <= 49) {
        arrayAgesPositive[4]++;
      } else if (e.age <= 59) {
        arrayAgesPositive[5]++;
      } else if (e.age <= 69) {
        arrayAgesPositive[6]++;
      } else if (e.age <= 79) {
        arrayAgesPositive[7]++;
      } else {
        arrayAgesPositive[8]++;
      }
    }
    for (let e of auxAgesNegative) {
      if (e.age <= 9) {
        arrayAgesNegative[0]++;
      } else if (e.age <= 19) {
        arrayAgesNegative[1]++;
      } else if (e.age <= 29) {
        arrayAgesNegative[2]++;
      } else if (e.age <= 39) {
        arrayAgesNegative[3]++;
      } else if (e.age <= 49) {
        arrayAgesNegative[4]++;
      } else if (e.age <= 59) {
        arrayAgesNegative[5]++;
      } else if (e.age <= 69) {
        arrayAgesNegative[6]++;
      } else if (e.age <= 79) {
        arrayAgesNegative[7]++;
      } else {
        arrayAgesNegative[8]++;
      }
    }
    console.log([arrayAgesPositive, arrayAgesNegative]);
    return [arrayAgesPositive, arrayAgesNegative];
  }

  getUnitIdByName(name: string): number {
    for (const u of this.units) {
      if (u.nome == name) {
        return u.idUnit;
      }
    }
  }

}
