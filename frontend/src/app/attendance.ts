import { Patient } from './patient';

import { HealthUnit } from './healthUnit';

export interface Attendance {
    _id?: string;
    idUnit: number;
    paciente: any;
    idAttendance: number;
    dateAttendance: Date;
    sexo?: string;
    time: number;
    possibContagio: boolean;
    resultTest1?: boolean;
    resultTest2?: boolean;
}
