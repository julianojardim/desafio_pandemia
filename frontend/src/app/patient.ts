export interface Patient {
    _id?: string;
    nome: string;
    cpf: string;
    sexo: string;
    numContato?: string;
    idade: number;
    peso?: number;
    altura?: number;
    adress?: {
        rua: string,
        numero: number,
        bairro: string,
        cidade: string
    };
}
