import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HealthUnit} from './healthUnit';
import {Attendance} from './attendance';
import {Patient} from './patient';
import {CookieService} from 'ngx-cookie-service';


@Injectable({
  providedIn: 'root'
})

export class ApiService {
  urlRoot = 'http://localhost:3100'; // url do server express

  idToken;
  options = {headers: new HttpHeaders().set('Content-type', 'application/json')};

  constructor(private http: HttpClient, private cookieService: CookieService) {
  }

  getUnits(): Observable<HealthUnit[]> {
    return this.http.get<any>(this.urlRoot + '/units', this.options).pipe(
      catchError(this.handleError)
    );
  }

  getAtts(): Observable<Attendance[]> {
    return this.http.get<any>(this.urlRoot + '/atts', this.options).pipe(
      catchError(this.handleError)
    );
  }
  getAttsofUnit(id, andamento?): Observable<Attendance[]> {
    let params = andamento ? {andamento}: {};
    return  this.http.get<Attendance[]>(this.urlRoot+'/attsofunit/'+id,{headers : this.options.headers, params}).pipe(
      catchError(this.handleError)
    );
  };
  editAtts(id, attendance):Observable<Attendance> {
    return  this.http.patch<Attendance>(this.urlRoot+ '/atts/' + id,attendance,this.options).pipe(
      catchError(this.handleError)
    );
  }
  getMaleAtts(): Observable<Attendance[]> {
    return this.http.get<any>(this.urlRoot + '/male', this.options).pipe(
      catchError(this.handleError)
    );
  }

  getFemaleAtts(): Observable<Attendance[]> {
    return this.http.get<any>(this.urlRoot + '/female', this.options).pipe(
      catchError(this.handleError)
    );
  }

  getAges(): Observable<any[]> {
    return this.http.get<any>(this.urlRoot + '/ages', this.options).pipe(
      catchError(this.handleError)
    );
  }

  setPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(this.urlRoot + '/patient', patient, this.options).pipe(
      catchError(this.handleError)
    );
  }

  getPatientId(id: string): Observable<Patient> {
    return this.http.get<Patient>(this.urlRoot + '/patient/' + id).pipe(
      catchError(this.handleError)
    );
  }

  getPatientCpf(cpfPatient: string): Observable<Patient> {
    return this.http.get<Patient>(this.urlRoot + '/patientcpf/' + cpfPatient, this.options).pipe(
      catchError(this.handleError)
    );
  }

  setAtts(att: Attendance): Observable<Attendance> {
    return this.http.post<Attendance>(this.urlRoot + '/atts', att, this.options).pipe(
      catchError(this.handleError)
    );
  }

  getTimes(id, pacient?: boolean): Observable<any[]> {
    let params;

    if(pacient) {
      params = {paciente: true};
    } else {
      params = {paciente: false};
    }

    return this.http.get<any>(this.urlRoot + '/time/' + id, {headers: this.options.headers, params}).pipe(
      catchError(this.handleError)
    );
  }

  getAtts2(): Observable<Attendance[]> {
    return this.http.get<any>(this.urlRoot + '/auth/atts2', this.options).pipe(
      catchError(this.handleError)
    );
  }

  postLogin(login) {
    return this.http.post<any>(this.urlRoot + '/auth/authenticate', login, this.options).pipe(
      catchError(this.handleError)
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  isLogged(){
    if(localStorage.getItem('token') == null){
      return false;
    }else{
      return true;
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      return throwError(error.status);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

}
