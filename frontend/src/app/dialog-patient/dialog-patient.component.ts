import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Attendance} from "../attendance";
import {ApiService} from "../service";

@Component({
  selector: 'app-dialog-patient',
  templateUrl: './dialog-patient.component.html',
  styleUrls: ['./dialog-patient.component.css']
})
export class DialogPatientComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:Attendance,
    private apiService:ApiService,
    private dialogRef: MatDialogRef<DialogPatientComponent>
    ) {
  console.log(data);
  }
  onClick(result:boolean){
    this.data.resultTest2 = result;
    this.apiService.editAtts(this.data._id,this.data).subscribe(
      ()=>{
        this.dialogRef.close(this.data._id);
      }
    )
    ;
  }


}
