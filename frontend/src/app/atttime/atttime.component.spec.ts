import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtttimeComponent } from './atttime.component';

describe('AtttimeComponent', () => {
  let component: AtttimeComponent;
  let fixture: ComponentFixture<AtttimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtttimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtttimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
