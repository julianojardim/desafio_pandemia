import { Component, OnInit,Input } from '@angular/core';


@Component({
  selector: 'app-atttime',
  templateUrl: './atttime.component.html',
  styleUrls: ['./atttime.component.css']
})
export class AtttimeComponent implements OnInit {
  @Input('timeMin') min:number;
  @Input('timeMed') med:number;
  @Input('timeMax') max:number;

  constructor() { }

  ngOnInit() {
  }

}
