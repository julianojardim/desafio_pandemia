import { CreateComponent } from './patient/create/create.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent as DashSaude} from './unidadeDeSaude/dashboard/dashboard.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import { AttendanceComponent } from './unidadeDeSaude/attendance/attendance.component';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'paciente/cadastrar', component: CreateComponent, canActivate: [AuthGuardService]},
  {path: 'saude', component: DashSaude, canActivate: [AuthGuardService], canLoad: [AuthGuardService]},
  {path: '', component: DashboardComponent},
  {path: 'saude/atendimento', component: AttendanceComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
