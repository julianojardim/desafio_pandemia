import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {isNull} from 'util';

const INPUT_PROVIDER: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CpfComponent),
  multi: true
};

@Component({
  selector: 'app-cpf',
  templateUrl: './cpf.component.html',
  styleUrls: ['./cpf.component.css'],
  providers: [INPUT_PROVIDER]
})
export class CpfComponent implements ControlValueAccessor {


  @Input() isReadOnly = false;
  private innerValue: any;

  constructor() {
  }

  get value() {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.value) {
      this.innerValue = v;
      this.onChangeCb(v);
      this.onTouchedCb(v);
      this.checkcpf(v);
    }
  }

  onChangeCb: (_: any) => void = () => {
  }
  onTouchedCb: (_: any) => void = () => {
  }


  registerOnChange(fn: any): void {
    this.onChangeCb = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCb = fn;
  }

  writeValue(obj: any): void {
    this.value = obj;

  }

  setDisabledState(isDisabled: boolean) {
    this.isReadOnly = isDisabled;
  }

  /**
   * Customiza o valor do cpf para ficar no formato 000.000.000-00
   * @param cpf
   */
  checkcpf(cpf): void {
    cpf = isNull(cpf) ? '' : cpf;
    if (cpf.length === 3) {
      cpf += '.';
      this.value = cpf;
    }
    if (cpf.length === 7) {
      cpf += '.';
      this.value = cpf;
    }
    if (cpf.length === 11) {
      cpf += '-';
      this.value = cpf;
    }
  }


}
