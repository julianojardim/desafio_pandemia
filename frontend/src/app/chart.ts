import {Chart} from 'node_modules/chart.js';
import {Injectable} from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class ApiChart {
    myChart;
    myChartSex;
    myChartAges;
    constructor() {
    }

    plotChart() {
          this.myChart = new Chart('myChart', {
            type: 'bar',
            data: {
              labels: ["Positivo", "Negativo"],
              datasets: [
                {
                  label: "Testes",
                  backgroundColor: ["rgba(133,246,133,0.75)", "rgba(246,133,133,0.75)"],
                  borderColor: ["rgba(33,216,206,1)", "rgba(129,170,247,1)"],
                  borderWidth: 1,
                  data: [0,0]
                }
              ]
            },
            options: {
              legend: { display: false },
              scales: {
              yAxes: [{ticks: {
                beginAtZero: true
            }}]}
            }
        });

    }
    plotChartSex() {
        this.myChartSex = new Chart('myChartSex', {
            type: 'bar',
            data: {
              labels: ['Casos por sexo'],
              datasets: [
                {
                  label: 'Masculino',
                  data: [0],
                  backgroundColor: 'rgba(33,216,206,0.75)',
                  borderColor: ["rgba(33,216,206,1)"],
                  borderWidth: 1,
                },
                {
                  label: 'Feminino',
                  data: [0],
                  backgroundColor: 'rgba(129,170,247,0.75)',
                  borderColor: ["rgba(129,170,247,1)"],
                  borderWidth: 1,
                }
              ]
            },
            options: {
              scales: {
                xAxes: [{ stacked: true }],
                yAxes: [{ stacked: true , ticks: {
                    beginAtZero: true
                }}]
              }
            }
          });

    }
    plotChartAges() {
        this.myChartAges = new Chart('myChartAges', {
        type: 'bar',
        data: {
            labels: ['0-9', '10-19', '20-29', '30-39', '40-49', '50-59', '60-69', '70-79', 'Mais de 80'],
            datasets: [
                {
                  label: 'Positivo',
                  data: [0,0,0,0,0,0,0,0,0],
                  backgroundColor: "rgba(133,246,133,0.75)",
                  borderColor: ["rgba(70,246,70,1)"],
                  borderWidth: 1,
                  order:2
                },
                {
                  label: 'Negativo',
                  data: [0,0,0,0,0,0,0,0,0],
                  backgroundColor: "rgba(246,133,133,0.75)",
                  borderColor: ["rgba(246,70,70,1)"],
                  borderWidth: 1,
                  order:1
                }
              ]
        },
        options: {
            scales: {
              xAxes: [{ stacked: true}],
              yAxes: [{ stacked: true }]
            }
          }
    });
}

    updateChart(positive:number,negative:number){

        this.myChart.data.datasets[0].data = [positive,negative];
        console.log('aqui',this.myChart.data.datasets[0].data);
        this.myChart.update();
    }
    updateChartS(male:number,female:number){
        console.log(male,female);
        this.myChartSex.data.datasets[0].data = [male];
        this.myChartSex.data.datasets[1].data = [female];
        this.myChartSex.update();
    }
    updateChartAges(ages:number[]){
      console.log('update chart ages');
        this.myChartAges.data.datasets[0].data = ages[0];
        this.myChartAges.data.datasets[1].data = ages[1];
        this.myChartAges.update();
    }
}
